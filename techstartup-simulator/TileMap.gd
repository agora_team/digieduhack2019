extends TileMap

onready var follower = get_node("follower")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
    if Input.is_action_just_released('ui_select'):
        self.visible = !self.visible
        get_node("../Node").visible = !self.visible
        follower.counter = (follower.counter + 1)%4
        follower.change_robot(follower.last_direction)