extends TextureRect

onready var assistant = get_node("../assistant")
onready var character = get_node("../character")
onready var furniture = get_node("../furniture")
var timer

# Called when the node enters the scene tree for the first time.
func _ready():
    timer = Timer.new()
    timer.connect("timeout",self,"_on_timer_timeout") 
    add_child(timer) #to process
    pass # Replace with function body.

func _unhandled_input(event):
    if event is InputEventKey:
        if event.pressed and event.scancode == KEY_1:
            self.set_texture(load("res://assets/office-background/WORK_stage01.jpg"))
        if event.pressed and event.scancode == KEY_2:
            self.set_texture(load("res://assets/office-background/WORK_stage02.jpg"))
        if event.pressed and event.scancode == KEY_3:
            self.set_texture(load("res://assets/office-background/WORK_stage03.jpg"))
        if event.pressed and event.scancode == KEY_4:
            self.set_texture(load("res://assets/office-background/WORK_stage04.jpg"))
            furniture.visible = false
        if event.pressed and event.scancode == KEY_5:
            self.set_texture(load("res://assets/office-background/WORK_stage05.jpg"))
        if event.pressed and event.scancode == KEY_S:
            character.get_node("success").visible = true;
            timer.start(3)
        if event.pressed and event.scancode == KEY_F:
            character.get_node("fail").visible = true;
            timer.start(3)
        if event.pressed and event.scancode == KEY_T:
            assistant.get_node("PopupDialog").popup(Rect2(590, 45, 131, 62))
            assistant.get_node("Sprite").flip_h = true
            timer.start(3) #to start

func _on_timer_timeout():
   character.get_node("success").visible = false;
   character.get_node("fail").visible = false;
   assistant.get_node("PopupDialog").hide()
   assistant.get_node("Sprite").flip_h = false
