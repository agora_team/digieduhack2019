extends Sprite

onready var player = get_node("../character")

var counter = -1

var lateral = preload("res://assets/Robot parts/Companion-lateral.png")
var up = preload("res://assets/Robot parts/Companion-front.png")
var back = preload("res://assets/Robot parts/Companion-back.png")


var lateral_wrong = preload("res://assets/Robot parts/Companion-wrong.png")
var up_wrong = preload("res://assets/Robot parts/Companion-wrong.png")
var back_wrong = preload("res://assets/Robot parts/Companion-wrong-back.png")

var last_direction = Vector2(0,0);

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var direction = player.follower_position - position
	change_robot(direction)
	
func change_robot(direction):
	
	if counter < 0:
		last_direction = direction
		return
		
	self.visible = true
	
	if counter < 2:
		if direction.x != 0:
			last_direction = direction
			self.set_texture(lateral_wrong)
		elif direction.y < 0:
			last_direction = direction
			self.set_texture(back_wrong)
		elif direction.y > 0:
			last_direction = direction
			self.set_texture(up_wrong)
	else:
		if direction.x != 0:
			last_direction = direction
			self.set_texture(lateral)
		elif direction.y < 0:
			last_direction = direction
			self.set_texture(back)
		elif direction.y > 0:
			last_direction = direction
			self.set_texture(up)
	
	position = player.follower_position
	
