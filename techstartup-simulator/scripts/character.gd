extends KinematicBody2D

onready var animation = get_node("Sprite/c_animation")

export (int) var speed = 3

var velocity = Vector2()
var follower_position = Vector2()

var follower_distance = 30

func _ready():
  pass

func get_input():
    velocity = Vector2()
    
    if Input.is_action_pressed('ui_right'):
        velocity.x += 1
        animation.play("right")
        follower_position = position - Vector2(follower_distance, 0)
    elif Input.is_action_pressed('ui_left'):
        velocity.x -= 1
        animation.play("left")
        follower_position = position - Vector2(-follower_distance, 0)
    elif Input.is_action_pressed('ui_down'):
        velocity.y += 1
        animation.play("down")
        follower_position = position - Vector2(0, follower_distance)
    elif Input.is_action_pressed('ui_up'):
        velocity.y -= 1
        animation.play("up")
        follower_position = position - Vector2(0, -follower_distance)
    else:
        animation.stop(false)
    velocity = velocity.normalized() * speed

func _physics_process(delta):
    get_input()
    move_and_collide(velocity)